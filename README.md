Achiiva Logger
==================

This module is for logging user logins, activities and sharing of activities
### Setup
    # add module dependency to package.json
    e.g. "achiiva-log": "git+http://angela.yamat@stash.insurtech.com.au/scm/ac/achiivalog.git"
    # create file (e.g. achiivalog.js) which will be the gateway to the achiiva-logger module.
     t will setup the model maps by passinh the model maps and search config (from development.json) to the init() function of the logger module
     
    achiivalog.init = function(config){
        logger.init(model_maps, config.get('search'), process.argv[2]);
    }

    If the third param in the logger.init() is set to "recreate", it will regenerate new model maps.

    # call include achiivalog.js in the app.js to call the init() once node service has started
    
    