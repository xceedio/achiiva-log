var achiivalog = require('../lib/achiiva-logger');
var test = require('unit.js');

var model_maps = {    
    "logins" : {
        "properties" : {
            "lo_userId" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "lo_name" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "lo_date_login" : {
                "type" : "date"
            },
            "lo_location_time_zone" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "lo_created_at" : {
                "type" : "date"
            },
            "timestamp" : {
                "type" : "date"
            }
        }
    },
    "sharing": {
        "properties" : {
            "sh_userId" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "sh_name" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "sh_shared_to_social_media" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "sh_shared_from_page" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "sh_shared_info_id" : {
                "type" : "string",
                "index": "not_analyzed"
            },
            "sh_shared_info_data" : {
                "type" : "object",
                "properties" : {
                    "activity" : { "type" : "string" },
                    "commentId" : { "type" : "string" },
                    "pageURL" : { "type" : "string" }
                 }
            },
            "sh_created_at" : {
                "type" : "date"
            },
            "timestamp" : {
                "type" : "date"
            }
        }
    }
};

var config = {
    "host": "172.30.1.5",
    "port": 9200,
    "indexName" : "achv_event_logs"
}
  

describe('setClient', function(){
    it('Init model maps', function(){
        achiivalog.init(model_maps, config, process.argv[2]);
    })
})