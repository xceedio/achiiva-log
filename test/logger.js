var achiivalog = require('../lib/achiiva-logger');
var test = require('unit.js');
var assert = require("assert")
var expect = require('expect.js');
var should = require('should');

var model_maps = {
    "logins": {
        "properties": {
            "lo_userId": {
                "type": "string",
                "index": "not_analyzed"
            },
            "lo_name": {
                "type": "string",
                "index": "not_analyzed"
            },
            "lo_date_login": {
                "type": "date"
            },
            "lo_location_time_zone": {
                "type": "string",
                "index": "not_analyzed"
            },
            "lo_created_at": {
                "type": "date"
            },
            "timestamp": {
                "type": "date"
            }
        }
    },
    "sharing": {
        "properties": {
            "sh_userId": {
                "type": "string",
                "index": "not_analyzed"
            },
            "sh_name": {
                "type": "string",
                "index": "not_analyzed"
            },
            "sh_shared_to_social_media": {
                "type": "string",
                "index": "not_analyzed"
            },
            "sh_shared_from_page": {
                "type": "string",
                "index": "not_analyzed"
            },
            "sh_shared_info_id": {
                "type": "string",
                "index": "not_analyzed"
            },
            "sh_shared_info_data": {
                "type": "object",
                "properties": {
                    "activity": {"type": "string"},
                    "commentId": {"type": "string"},
                    "pageURL": {"type": "string"}
                }
            },
            "sh_created_at": {
                "type": "date"
            },
            "timestamp": {
                "type": "date"
            }
        }
    }
};

var config = {
    "host": "172.30.1.5",
    "port": 9200,
    "indexName": "achv_event_logs"
}



describe('Log Contructor', function() {
    before(function(){
        
    })

    it('should have correct values of elasticsearch host and port', function(done) {
        var config = {
            "host": "172.30.1.5",
            "port": 9200,
            "indexName": "achv_event_logs"
        }
        achiivalog.setClient(config)
        achiivalog.checkConnection()
        done()
    });

    it('should not accept blank index name', function(done){
        var config = {
            "host": "172.30.1.5",
            "port": 9200,
            "indexName": "achv_event_logs"
        }
        achiivalog.setClient(config)
        achiivalog.checkConnection()
        done()
    })        
})




describe('Log Data', function(){
    
    var config = {
        "host": "172.30.1.5",
        "port": 9200,
        "indexName": "achv_event_logs"
    }
        
    before(function(){
        achiivalog.setClient(config)
    })
    
    
    
    it( 'should only accept a valid type', function(done){
        achiivalog.logit("logins", {}, function(){});        
        done()
    })
    
    it( 'for Login: should only accept valid JSON with valid fields', function(done){
        achiivalog.logit(config.indexName, "logins", {
            lo_userId: 1,
            lo_date_login: new Date(),
            lo_location_time_zone: "Asia/Manila",
            lo_created_at: new Date(),
            timestamp: new Date(),
            lo_name: "Fname Lname"
        }, function(res){
            console.log(res)
            res.should.not.have.property('error')
            
            done();
        });  
        
    })
    
    
    it( 'for sharing activity: should only accept valid JSON with valid fields', function(done){
        achiivalog.logit(config.indexName, "sharing", {
            sh_userId: "1",
            sh_name:  "Fname Lname",
            sh_shared_to_social_media: "facebook",
            sh_shared_from_page: 'goal_details',
            sh_shared_info_id: "1",
            sh_shared_info_data: {
                activity : 'GOAL',
                commentId : "123",
                pageURL : ""                
            },
            sh_created_at: "2015-02-09",
            timestamp: "2015-02-09"
        }, function(err){
            console.log(err)
            //expect(res).to.have.deep.property("error");
            //res.error.should.not.be.ok
            test.should.not.exist(err.error, "error" )
            //res.should.not.have.property('error', "Error:" +  res.error)
            
            done();
        });  
        
    })
})
