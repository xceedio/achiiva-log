var elasticsearch = require('elasticsearch');
var async = require('async');
var client;
var config;
var model_maps = {};
var settings = {
    "number_of_shards": 2
};
var default_mapping = {
    "_default_": {
        "_timestamp": {
            "enabled": true,
            "store": true,
            "format": 'yyyy-MM-dd HH:mm:ss'
        },
        "dynamic": "strict"
    }
};

//**********************************************************public functions**********************************************************//
/**
 * Function that will set the elastic search client
 * @param config object
 */
exports.setClient = function (config) {
    _setClient(config);
};

exports.init = function (_model_maps, config, extraSettings, callback) {
     model_maps = _model_maps;
    for (var attrname in extraSettings) { settings[attrname] = extraSettings[attrname]; }
    _setClient(config);
     el.create(config.recreate, function(){
        
        callback();
    });
};

exports.logit = function (indexName, type, data, callback) {
    checker(indexName, type, function (err, exists) {
        if (exists === true) {
            createLog(indexName, type, data, function (err) {
                callback(err)
            });
        } else {
            throw Error('Index does not exist')
            //console.log('Type [ ' + type + ' ] does not exist!');
        }
    });
};

exports.checkConnection = function () {
    // check connection
    client.ping({
        requestTimeout: 1000,
        // undocumented params are appended to the query string
        hello: "elasticsearch!"
    }, function (error) {
        if (error) {
            //console.log('elasticsearch cluster is down!');
            throw "Cannot connect to Elasticsearch"
        }
    });


    if (config.indexName == '') {
        throw "index name cannot be blanked"
    }
}

exports.search = function ( indexName, options, callback) {
    var basic = { index: indexName } ; 
    var serachobj = extend({}, basic, options );
    client.search(serachobj
        , function (error, response) {
        callback(error, response);
    });
}

exports.close = function () {
    client.close();
}

exports.update = function (indexName, type, id, update, callback) {
    client.update({
        index: indexName,
        type: type,
        id: id,
        body: {
            // put the partial document under the `doc` key
            doc: update
        }
    }, function (error, response) {
        callback(error, response);
    })
}

exports.updateByScript = function (indexName, type, id, upsert, script, params, callback) {
    client.update({
        index: indexName,
        type: type,
        id: id,
        refresh : true,
        body: {            
            script: script,
            upsert: upsert,
            params: params
        }
    }, function (error, response) {
        callback(error, response);
    });
};

exports.bulk = function (options, callback) {
    client.bulk({
        body:options,
        refresh : true
    }, function(error, response){
        callback(error, response);
    });
}


exports.count = function(indexName, options ,callback){
    var basic = { index: indexName } ; 
    var searchObj = extend({}, basic, options );
    client.count(searchObj, function(error, response){
        callback(error, response);
    });
};

exports.deleteByQuery = function(indexName, options ,callback){
    var basic = { index: indexName } ; 
    var searchObj = extend({}, basic, options );
    client.deleteByQuery(searchObj, function(error, response){
        callback(error, response);
    });
};

exports.getClient = function () {
    return client;
}
//**********************************************************end of public functions**********************************************************//


var _setClient = function (_config) {
    // set config
    config = _config;
    client = new elasticsearch.Client({
        host: config.host + ":" + config.port,
        keepAlive: true
    });
}


var checker = function (indexName, type, callback) {
    client.indices.existsType({
        index: indexName,
        type: type
    }, function (error, exists) {
        callback(error, exists);
    });
};

var createLog = function (indexName, type, data, callback) {
    client.indices.getMapping({
        index: indexName,
        type: type
    }, function (err, res) {
        var obj = res[indexName]["mappings"][type]['properties'];
        var err = [];
        var mapdata = {};
        for (var o in obj) {
            if (!data.hasOwnProperty(o)) {
                err.push(o);
            } else {
                mapdata[o] = data[o];
            }
        }
        if (err.length === 0) {
            client.create({
                index: indexName,
                type: type,
                body: mapdata
            }, function (err, resp) {
                if (err) {
                    //console.log(err);
                    callback({error: err});
                } else {
                    //console.log(resp);
                    callback(resp);
                }
            });

        } else {
            callback({error: err});
        }
    });
}


var el = {
    create: function (stat, callback) {
        
        client.indices.exists({
            index: config.indexName
        }, function (err, resp) {
            
            var mappings = extend({}, model_maps, default_mapping);
            if (stat === true) {
                 console.log("----->>>RESETTING: " + config.indexName);
                el.delete(function (err, resp) {
                    //Generate the Models
                    client.indices.create({
                        index: config.indexName,
                        body: {
                            "settings": settings,
                            "mappings": mappings
                        }
                    }, function (err, resp) {
                        if (resp) {

                            console.log("----->>>CREATE NEW AGAIN: " + config.indexName);
                            console.log(resp);                            
                        } else {
                            console.log(err);
                        }
                        callback();
                    });
                });
            } else if (resp === false) {                
                //Generate the Models
                client.indices.create({
                    index: config.indexName,
                    body: {
                        "settings": settings,
                        "mappings": mappings
                    }
                }, function (err, resp) {
                    if (resp) {
                        console.log("----->>>CREATE ONCE: " + config.indexName);
                        console.log(resp);                        
                    } else {
                        console.log(err);
                    }
                    callback();
                });
            } else if (resp === true) {
                 el.update_map_types(function(){
                     callback();
                });
            }
        });
    },
    update_map_types: function (callback) {
        var mapArray = [];
        for (m in model_maps) {
            var obj = {}
            obj[m] = model_maps[m];
            var map_individual = extend({}, obj[m], default_mapping['_default_']);
            mapArray.push({
                index: config.indexName,
                type: m,
                body: map_individual
            });
        }
        
        async.each(mapArray, function(map, callback){
            client.indices.putMapping(map, function (err, resp) {
                if (resp) {
                    console.log("----->>>MAP TYPES CHECKING AND UPDATE " + map.index);
                    console.log(resp);                    
                } else {
                    console.log(err);
                }
                callback();
            });
        }, function(err){
            callback();
        });
    },
    delete: function (callback) {
        //Generate the Models
        client.indices.delete({
            index: config.indexName
        }, function (err, resp) {
            callback(resp, err);
        });
    }
};

function extend(target) {
    var sources = [].slice.call(arguments, 1);
    sources.forEach(function (source) {
        for (var prop in source) {
            target[prop] = source[prop];
        }
    });
    return target;
}